import * as webpack from 'webpack';
import * as path from 'path';
import * as precss from 'precss';
import * as autoprefixer from 'autoprefixer';
import * as HtmlWebpackPlugin from 'html-webpack-plugin';
import { CheckerPlugin, TsConfigPathsPlugin } from 'awesome-typescript-loader';
import { AureliaPlugin } from 'aurelia-webpack-plugin';
import * as ManifestPlugin from 'webpack-plugin-manifest';


export default (env={}) : webpack.Configuration => ({
    entry: {
        "main": ["aurelia-bootstrapper"],
    },
    target: "electron-renderer",
    output: {
        path: path.resolve(__dirname, "build"),
        filename: "[name].[hash].js"
    },
    resolve: {
        extensions: [".ts", ".js"],
        modules: [path.resolve(__dirname, "src"), "node_modules"]
    },
    module:{
        rules:[
            {
                test: /\.css$/i,
                use: [
                    'css-loader',
                    {
                        loader: 'postcss-loader',
                        options: {
                            plugins: () => [precss, autoprefixer({browsers: ['last 2 versions']})]
                        }
                    }
                ]
            },
            {
                test: /\.ts$/i,
                use: 'awesome-typescript-loader',
                exclude: [path.resolve(__dirname, 'node_modules'), path.join(__dirname, "index.ts")]
            },
            { test: /\.html$/i, loader: 'html-loader' },
        ]
    },
    plugins:[
        new AureliaPlugin(),
        new HtmlWebpackPlugin({
            template: "src/main.ejs",
            title: "E300",
            filename: "index.html"
        }),
        new webpack.ProvidePlugin({

        }),
        new ManifestPlugin(),
        new CheckerPlugin(),
        new TsConfigPathsPlugin(),
    ]
})
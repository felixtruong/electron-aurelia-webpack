const {
  series,
  concurrent,
  rimraf,
  crossEnv
} = require('nps-utils');

module.exports = {
  scripts: {
    default: "nps dev",
    dev: concurrent({
      server: 'webpack-dev-server -d --inline  --hot',
      electron: series(
        'tsc',
        `wait-on http://localhost:8080`,
        'electron . --inspect=5858 --enable-logging --remote-debugging-port=9222'
      ) + " --kill-others"
    })
  }
}
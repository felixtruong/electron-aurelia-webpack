import { BrowserWindow, app } from 'electron';

let dev = true;

app.on('window-all-closed', () => {
    process.platform != 'darwin' && app.exit(1)
})


app.on('ready', () => {
    var main_width = 800;
    var main_height = 600;
  
    var mainWindow = new BrowserWindow({
      width: main_width,
      height: main_height,
      frame: dev, //Dev only
      webPreferences: {
        plugins: true,
        devTools: true, // TODO: set to false for production
        nodeIntegration: true
      },
    });
  
    mainWindow.loadURL(dev ?
      'http://localhost:8080' :
      'file://' + __dirname + '/build/index.html'
    );
  
    mainWindow.on('closed', function () {
    });
})